ï»¿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Context : DbContext

//Pour l'approche CodeFirst: base("")
//Pour l'approche DataBaseFirst: base("Nom de BD ")
{
    public Context() : base("TP_C#") {}

    public DbSet<Company> Companies { get; set; }
    public DbSet<User> Users { get; set; }

    //Using Fluent API

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .Property(e => e.Username)
            .IsRequired();
        modelBuilder.Entity<User>()
           .Property(e => e.Password)
           .IsRequired();
    }
}



    
