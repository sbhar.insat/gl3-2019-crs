using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* For tracking the Evolution of the database , we can use EF Migration.
 * --to add a new migration --
[Package Manager console]
add-migration <name of migration>
Exple : if we want to add a new column "Phone number" to model "user" --> add-migration AddPhone
        then we define the migration : 

            public partial class AddPhone : PhoneDbMigration
            {
                public override void Up()
                {
                    AddColumn("TP_C#.User", "PhoneNumber", c => c.String());
                }

                public override void Down()
                {
                    DropColumn("TP_C#.User", "PhoneNumber");
                }
            }
        }

to update the Database with the new model --> Update-Database
*/

class Program
{
    static void Main(string[] args)
    {
        using (var db = new Context())
        { 
            // Create and save a new Company
            Console.Write("Enter an ID for a new Company :");
            var id = Console.ReadLine();

            Console.Write("Enter a name for a new Company :");
            var name = Console.ReadLine();

            var company = new Company {CompanyId=id,CompanyName = name };
            db.Companies.Add(company);
            db.SaveChanges();

            // Display all Companies from the database using LINQ Expressions
            var query = from b in db.Companies
                        orderby b.CompanyId
                        select b;



            Console.WriteLine("All Companies in the database:");
            foreach (var item in query)
            {
                Console.WriteLine(item.CompanyId + " " +item.CompanyName);
            }

            //Delete a Company by it's ID
            Console.Write("Enter the ID of the Company that you want de delete :");
            var delete_id = Console.ReadLine();

            Company cpy = db.Companies.Single(c => c.CompanyId == delete_id);
            db.Companies.Remove(cpy);
            db.SaveChanges();

            //Creating a new User and adding it to the DB
            Console.Write("Enter an ID for a new User :");
            var id_user = Console.ReadLine();

            Console.Write("Enter a name for a new User :");
            var name_user = Console.ReadLine();

            Console.Write("Enter a password for a new User :");
            var pass_user = Console.ReadLine();

            var user = new User { Id = id_user, Username = name_user, Password = pass_user };
            db.Users.Add(user);
            db.SaveChanges();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
