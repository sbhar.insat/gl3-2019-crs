﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity2
{
    public static class Extensions
    {
        public static string retireCar(this string str, char a)
        {
            return str.Remove(str.IndexOf(a), 1);
        }

    }
}
