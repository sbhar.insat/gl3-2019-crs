﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Activity1
{
    class Student
    {
        public string name { get; set; }
        public uint age { get; set; }

        public override string ToString()
        {
            return "Student\nname = " + name + "\nage = " + age; 
        }
    }
}
